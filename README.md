# README #

* [Acknowledgment](#markdown-header-acknowledgment)
* [License](./LICENSE.txt)

#CONFIGURATION
Now we need to turn to configuration of the project to work with the server process already configured.

### Step 1: Edit Config File ###
Using whatever text editor you prefer, edit the configuration file located in the __config__ directory of this project. 

In editing the config, you will need to tell the program where to find the server process. This is configured using two different configuration values. The first is the address of the server:

	<servername value="localhost" />

Here you will place the hostname or IP address of the host where the server process is located. Then the port number that the server process is listening on is stated in:

	<portnum value="5000" />

Now the number of threads that will be used by the worker process while calculating the parameters is configured using:

	<threads max="1" />

The number of threads equates to how many images will be processed at the same time by this worker process.  It is not advised to set this number to a higher value than the number of threads available on the machine this process is running on.

Next, is to decide if the server process shall pull images from a local directory or from the Helioviewer.org API.  If processing from the local file system, then the the following three settings need to be set.

First is to indicate that the process is using the local file system:

	<uselocal value="true" />

The Second is to specify how many directories the process will cache the contents of. This caching process is only for the paths and not for the actual files: 

	<cachesize max="300" />

The base directory of where to find the directories that contain the JP2 files:

	<basedir value="/data/AIA_JP2" />


The logger entry is where to find the config file for the logging library and what the name of the config file is. Here "loc" is the directory relative to running location and "name" is the name of the config file.

	<logger loc="config" name="log4j2.xml" />


#Acknowledgment

 This work was supported in part by two NASA Grant Awards (No. NNX11AM13A, and No. NNX15AF39G),
 and one NSF Grant Award (No. AC1443061). The NSF Grant Award has been supported by funding 
 from the Division of Advanced Cyberinfrastructure within the Directorate for Computer and 
 Information Science and Engineering, the Division of Astronomical Sciences within the 
 Directorate for Mathematical and Physical Sciences, and the Division of Atmospheric and 
 Geospace Sciences within the Directorate for Geosciences.
 
 
====================================================================
 
© 2018 Dustin Kempton, Rafal Angryk
 
[Data Mining Lab](http://dmlab.cs.gsu.edu/), 
[Georgia State University](http://www.gsu.edu/)