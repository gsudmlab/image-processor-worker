/**
 * image-processor-worker, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.imgproc.interfaces;

import java.awt.image.BufferedImage;

/**
 * The public interface for the classes that produce the set of image parameter
 * values for an imput image.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public interface IImageParamsCalculator {

	/**
	 * Calculates the image parameters for the input image.
	 * 
	 * @param img The image to calculate the parameters on.
	 * @return A set of 1 to N parameters calculated over a grid of cells.
	 */
	public double[][][] getParameters(BufferedImage img);

	/**
	 * The number of parameters the implementation will return when an Image is
	 * passed in.
	 * 
	 * @return The number of parameters the implementation will return
	 */
	public int getNumParams();
}
