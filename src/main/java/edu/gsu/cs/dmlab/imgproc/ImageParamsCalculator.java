/**
 * image-processor-worker, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.imgproc;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import edu.gsu.cs.dmlab.imageproc.imageparam.interfaces.IParamCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.util.BufferedImage2Matrix;
import edu.gsu.cs.dmlab.imgproc.interfaces.IImageParamsCalculator;

/**
 * Class that produce a set of image parameters for an input image. The number
 * of parameters calculated for each cell is determined by the number of
 * parameter calculators passed in through the constructor.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ImageParamsCalculator implements IImageParamsCalculator {

	IParamCalculator[] calculators;

	/**
	 * Constructor that takes in the array of parameter calculators. Each calculator
	 * is responsible for calculating a specific parameter value for the cells of
	 * the image. All calculators must produce the same number of cells.
	 * 
	 * @param calculators The array of parameter calculators to apply to an input
	 *                    image.
	 */
	public ImageParamsCalculator(IParamCalculator[] calculators) {
		if (calculators == null | calculators.length == 0)
			throw new IllegalArgumentException(
					"The list of parameter calculators cannot be null or empty in ImageParamsCalculatorOne constructor.");
		this.calculators = calculators;
	}

	public double[][][] getParameters(BufferedImage img) {

		// This is supposed to calculate each of the parameters in parallel and
		// collect the results into a list in the same order that the
		// calculators are presented in the input.
		double[][] inputArr = BufferedImage2Matrix.get2DArrayFromImage(img);
		List<double[][]> paramsList = IntStream.range(0, calculators.length).mapToObj(s -> {
			return calculators[s].calculateParameter(inputArr);
		}).collect(Collectors.toList());

		int xLen = paramsList.get(0)[0].length;
		int yLen = paramsList.get(0).length;

		// Now copy all the results into one array and let's return it.
		double[][][] results = new double[yLen][xLen][this.calculators.length];
		for (int i = 0; i < paramsList.size(); i++) {
			double[][] paramArr = paramsList.get(i);
			for (int y = 0; y < paramArr.length; y++) {
				for (int x = 0; x < paramArr[0].length; x++) {
					results[y][x][i] = paramArr[y][x];
				}
			}
		}
		paramsList.clear();

		return results;
	}

	public int getNumParams() {
		return this.calculators.length;
	}
}
