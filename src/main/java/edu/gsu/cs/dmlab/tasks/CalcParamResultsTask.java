/**
 * image-processor-worker, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.tasks;

import java.awt.image.BufferedImage;
import java.util.concurrent.Callable;

import edu.gsu.cs.dmlab.datasources.interfaces.IImageDataSource;
import edu.gsu.cs.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.cs.dmlab.datatypes.ParamId;
import edu.gsu.cs.dmlab.datatypes.ParamResults;
import edu.gsu.cs.dmlab.imgproc.interfaces.IImageParamsCalculator;

/**
 * The a task that is used to process an image to be returned to the server
 * process for insertion into the database.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class CalcParamResultsTask implements Callable<ParamResults> {

	IImageDataSource source;
	IImageParamsCalculator calculator;
	ParamId paramId;

	/**
	 * Constructor for the parameter calculation task
	 * 
	 * @param source     The object that will provide the image that is to be
	 *                   processed and then returned by this task
	 * 
	 * @param calculator The parameter calculator that will perform the parameter
	 *                   calculation
	 * 
	 * @param paramId    The descriptor of the image that needs to be processed by
	 *                   this task
	 */
	public CalcParamResultsTask(IImageDataSource source, IImageParamsCalculator calculator, ParamId paramId) {
		this.source = source;
		this.calculator = calculator;
		this.paramId = paramId;
	}

	@Override
	public ParamResults call() throws Exception {

		// Get Image and calculate parameters for it.
		ImageDBFitsHeaderData header = this.source.getHeader(this.paramId.time, this.paramId.wavelength);
		if (header != null) {
			BufferedImage img = this.source.getImage(this.paramId.time, this.paramId.wavelength);
			if (img != null) {
				double[][][] params = this.calculator.getParameters(img);

				// Set the result object to send back
				ParamResults results = new ParamResults();
				results.parameters = params;
				results.id = this.paramId.id;
				results.time = this.paramId.time;
				results.img = img;
				results.X0 = header.X0;
				results.Y0 = header.Y0;
				results.DSUN = header.DSUN;
				results.R_SUN = header.R_SUN;
				results.CDELT = header.CDELT;
				results.QUALITY = header.QUALITY;

				return results;
			}
		}

		return null;
	}

}
