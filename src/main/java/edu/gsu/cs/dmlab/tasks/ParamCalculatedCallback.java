/**
 * image-processor-worker, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.tasks;

import com.google.common.util.concurrent.FutureCallback;

import edu.gsu.cs.dmlab.datatypes.ParamResults;
import edu.gsu.cs.dmlab.service.interfaces.IWorkerTaskSupervisor;

/**
 * The callback object that is called by the executor when the parameter
 * calculation task has completed. This is a wrapper to allow a single class to
 * implement methods for handling the callback from multiple different future
 * tasks (I.E. the Supervisor).
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ParamCalculatedCallback implements FutureCallback<ParamResults> {

	IWorkerTaskSupervisor supervisor;

	/**
	 * Constructor
	 * 
	 * @param supervisor The supervisor class that will be called back through this
	 *                   class.
	 */
	public ParamCalculatedCallback(IWorkerTaskSupervisor supervisor) {
		this.supervisor = supervisor;
	}

	@Override
	public void onFailure(Throwable arg0) {
		this.supervisor.handleParamsCalculatedFailed(arg0);
	}

	@Override
	public void onSuccess(ParamResults results) {
		this.supervisor.handleParamsCalculated(results);
	}

}
