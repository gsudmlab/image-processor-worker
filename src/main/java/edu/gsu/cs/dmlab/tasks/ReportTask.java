/**
 * image-processor-worker, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.tasks;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.Buffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import edu.gsu.cs.dmlab.datatypes.ParamResults;

/**
 * The a task that is used to return results to the server process.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ReportTask implements Callable<Boolean> {

	ParamResults results;
	SocketAddress serverAddr;

	/**
	 * Constructor for the task that is used to communicate with the server process
	 * and return the parameters and image calculated by this process
	 * 
	 * @param results    The results to return to the server process
	 * 
	 * @param portNum    The port to communicate with the server process on
	 * 
	 * @param serverName The hostname or address used to connect to the server
	 *                   process
	 */
	public ReportTask(ParamResults results, int portNum, String serverName) {
		this.results = results;
		this.serverAddr = new InetSocketAddress(serverName, portNum);
	}

	@Override
	public void finalize() {
		this.results.parameters = null;
		this.results = null;
	}

	@Override
	public Boolean call() throws Exception {
		Boolean result = Boolean.FALSE;

		// Open Connection to server to send results.
		try (AsynchronousSocketChannel client = AsynchronousSocketChannel.open()) {

			Future<Void> connectResult = client.connect(this.serverAddr);
			connectResult.get(20, TimeUnit.SECONDS);

			// Send server code saying we wish to send results.
			int code = 2;
			byte[] data = Integer.valueOf(code).toString().getBytes();
			ByteBuffer buff = ByteBuffer.wrap(data);

			Future<Integer> writeResult = client.write(buff);
			writeResult.get(20, TimeUnit.SECONDS);

			// Read code back from server to see that it is ready.
			((Buffer) buff).flip();
			Future<Integer> readResult = client.read(buff);

			// Make sure we have data to read.
			int bytesRead = readResult.get(20, TimeUnit.SECONDS);
			if (bytesRead == 1) {

				((Buffer) buff).flip();
				byte[] lineBytes = new byte[bytesRead];
				buff.get(lineBytes, 0, bytesRead);

				// Check code we got back from server
				int eventId = Integer.parseInt(new String(lineBytes));
				if (eventId == 2) {

					// Write object out to server.
					try (ByteArrayOutputStream bos = new ByteArrayOutputStream()) {
						try (ObjectOutput out = new ObjectOutputStream(bos)) {
							out.writeObject(this.results);
							out.flush();
						}
						byte[] byteArr = bos.toByteArray();

						buff = ByteBuffer.wrap(byteArr);
						int totalWritten = 0;
						while (totalWritten < byteArr.length) {
							writeResult = client.write(buff);
							int numWritten = writeResult.get(20, TimeUnit.SECONDS);
							totalWritten += numWritten;
						}
					}
					result = Boolean.TRUE;
				}
			}

		}

		return result;
	}

}
