/**
 * image-processor-worker, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.tasks;

import java.io.ByteArrayInputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import edu.gsu.cs.dmlab.datatypes.ParamId;

/**
 * The a task that is used to communicate with the server process and request a
 * new image descriptor of an image that needs to be processed.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class FetchParamIdTask implements Callable<ParamId> {

	SocketAddress serverAddr;

	/**
	 * Constructor for the paramid fetch task
	 * 
	 * @param portNum    The port number to communicate with the server process on
	 * 
	 * @param serverName The hostname or address used to connecto to the server
	 *                   process
	 */
	public FetchParamIdTask(int portNum, String serverName) {
		this.serverAddr = new InetSocketAddress(serverName, portNum);
	}

	@Override
	public ParamId call() throws Exception {

		ParamId paramId = null;

		try (AsynchronousSocketChannel client = AsynchronousSocketChannel.open()) {
			Future<Void> connectResult = client.connect(serverAddr);

			// Wait until we are connected or timed out.
			connectResult.get(20, TimeUnit.SECONDS);

			// Write code to server saying we want an Id
			int code = 1;
			byte[] data = Integer.valueOf(code).toString().getBytes();
			ByteBuffer buff = ByteBuffer.wrap(data);
			Future<Integer> writeResult = client.write(buff);
			writeResult.get(20, TimeUnit.SECONDS);

			// Read back from the server that it is going to send us what we
			// want.
			((Buffer) buff).flip();
			Future<Integer> readResult = client.read(buff);

			// Make sure we have data to read.
			int bytesRead = readResult.get(20, TimeUnit.SECONDS);
			if (bytesRead == 1) {

				// Get the code from the returned results
				((Buffer) buff).flip();
				byte[] lineBytes = new byte[bytesRead];
				buff.get(lineBytes, 0, bytesRead);
				int eventId = Integer.parseInt(new String(lineBytes));

				// If it is what we sent, then we proceed.
				if (eventId == 1) {

					ByteBuffer byteBuffer2 = ByteBuffer.allocate(2048);

					// need to wait for result before we can process,
					// otherwise we get an error.
					readResult = client.read(byteBuffer2);
					readResult.get(20, TimeUnit.SECONDS);

					// Make sure we have data to read, if not, then there were
					// no more ids to give.
					if (byteBuffer2.position() > 0) {
						try (ByteArrayInputStream bis = new ByteArrayInputStream(byteBuffer2.array())) {
							try (ObjectInput in = new ObjectInputStream(bis)) {
								paramId = (ParamId) in.readObject();
							}
							//System.out.println("Time: " + paramId.time);
							//System.out.println("Wave: " + paramId.wavelength);
						}
					}
				}

			}

		}
		return paramId;
	}

}
