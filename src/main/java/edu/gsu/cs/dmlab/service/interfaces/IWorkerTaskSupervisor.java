/**
 * image-processor-worker, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.service.interfaces;

import edu.gsu.cs.dmlab.datatypes.ParamId;
import edu.gsu.cs.dmlab.datatypes.ParamResults;

/**
 * The public interface of the class that will handle the callback from various
 * tasks that are performed by this process.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public interface IWorkerTaskSupervisor {

	/**
	 * The method that is called once the descriptor for an image that needs to be
	 * processed is returned from the server process.
	 * 
	 * @param paramId The descriptor of the image to be processed. If null, then it
	 *                will be assumed no images are currently in need to be
	 *                processed and this process will pause for a period of time.
	 */
	public void handleParamIdFetched(ParamId paramId);

	/**
	 * The method that is called when the request for an image descriptor failed in
	 * some way.
	 * 
	 * @param arg0 The exception that was thrown, probably wrapped in another by the
	 *             executor.
	 */
	public void handleParamFetchFailed(Throwable arg0);

	/**
	 * The method called when the image parameters were successfully returned to the
	 * server process.
	 * 
	 * @param done A simple yes/no indicator indicating the success of the task
	 *             execution.
	 */
	public void handleParamsReported(Boolean done);

	/**
	 * The method that is called when reporting results to the server process failed
	 * in some way.
	 * 
	 * @param arg0 The exception that was thrown, probably wrapped in another by the
	 *             executor
	 */
	public void handleParamsReportedFailed(Throwable arg0);

	/**
	 * The method called once the parameter calculation task has completed and
	 * results are ready to be returned to the server process.
	 * 
	 * @param results The results that are to be returned to the server process.
	 */
	public void handleParamsCalculated(ParamResults results);

	/**
	 * The method that is called when processing of the image failed in some way.
	 * 
	 * @param arg0 The exception that was thrown, probably wrapped in another by the
	 *             executer
	 */
	public void handleParamsCalculatedFailed(Throwable arg0);
}
