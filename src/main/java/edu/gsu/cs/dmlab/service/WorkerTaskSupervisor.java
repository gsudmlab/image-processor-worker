/**
 * image-processor-worker, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.service;

import java.net.ConnectException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import edu.gsu.cs.dmlab.config.ConfigReader;
import edu.gsu.cs.dmlab.datasources.interfaces.IImageDataSource;
import edu.gsu.cs.dmlab.datatypes.ParamId;
import edu.gsu.cs.dmlab.datatypes.ParamResults;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.exceptions.InvalidConfigException;
import edu.gsu.cs.dmlab.imgproc.ImageParamsCalculator;
import edu.gsu.cs.dmlab.imgproc.interfaces.IImageParamsCalculator;
import edu.gsu.cs.dmlab.service.interfaces.IWorkerTaskSupervisor;
import edu.gsu.cs.dmlab.tasks.CalcParamResultsTask;
import edu.gsu.cs.dmlab.tasks.FetchParamIdTask;
import edu.gsu.cs.dmlab.tasks.ParamCalculatedCallback;
import edu.gsu.cs.dmlab.tasks.ParamIdFetchedCallback;
import edu.gsu.cs.dmlab.tasks.ParamReportedCallback;
import edu.gsu.cs.dmlab.tasks.ReportTask;

/**
 * The supervisor of all the things in this process. It requests descriptors of
 * images that need to be processed and runs tasks to process the images to
 * produce parameter values. I then returns the processed results and requests
 * more descriptors of images that need to be processed. If none are returned,
 * it waits for a period of time before querying again because it is assumed
 * that there currently are none to process. Like I said it does all the things.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class WorkerTaskSupervisor implements IWorkerTaskSupervisor {

	ConfigReader config;
	IImageDataSource dataSource;

	ListeningExecutorService comExecutor;
	ListeningExecutorService reportComExecutor;
	ListeningExecutorService calcExecutor;

	LinkedList<Future<ParamId>> fetchingTaskList;
	LinkedList<Future<Boolean>> reportingTaskList;
	LinkedList<Future<ParamResults>> calculatingTaskList;

	Map<Waveband, IImageParamsCalculator> calculators;

	Lock lock;
	Condition notFull;
	Condition notEmpty;
	ReentrantLock noDataLock;
	Condition waitingOnData;
	boolean noData;
	private Logger logger;

	/**
	 * Constructor
	 * 
	 * @param config The class that reads the configuration from the config file
	 * 
	 * @throws InvalidConfigException When the config reader encounters an error
	 *                                reading the config file
	 */
	public WorkerTaskSupervisor(ConfigReader config) throws InvalidConfigException {

		this.config = config;

		this.dataSource = config.getDataSource();

		int maxThreads = config.getMaxThreads();
		this.comExecutor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(maxThreads * 2));
		this.reportComExecutor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(maxThreads * 2));
		this.calcExecutor = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(maxThreads * 2));

		this.calculatingTaskList = new LinkedList<Future<ParamResults>>();
		this.reportingTaskList = new LinkedList<Future<Boolean>>();
		this.fetchingTaskList = new LinkedList<Future<ParamId>>();

		this.calculators = new HashMap<Waveband, IImageParamsCalculator>();
		for (Waveband wavelength : this.config.getWavelengths()) {
			IImageParamsCalculator calculator = new ImageParamsCalculator(this.config.getCalculators(wavelength));
			this.calculators.put(wavelength, calculator);
		}

		this.lock = new ReentrantLock();
		this.notFull = this.lock.newCondition();
		this.notEmpty = this.lock.newCondition();
		this.noDataLock = new ReentrantLock();
		this.waitingOnData = this.noDataLock.newCondition();
		this.noData = false;
		this.logger = LoggerFactory.getLogger(WorkerTaskSupervisor.class);
	}

	public void run() throws Exception {
		while (true) {
			this.addTask();
		}
	}

	private void addTask() throws Exception {

		// If we haven't hit the no data condition, then process as usual.
		if (!this.noData) {
			this.lock.lock();
			try {

				// If we are fetching or calculating maxThreads number of parameters, we should
				// wait until the not full condition has been called.
				while ((fetchingTaskList.size() + calculatingTaskList.size() + reportingTaskList.size()) >= (this.config
						.getMaxThreads())) {
					this.notFull.await(20, TimeUnit.SECONDS);
				}

				// Create a fetch Id task and submit it to the communication executer
				Callable<ParamId> fetchTask = new FetchParamIdTask(this.config.getPortNum(),
						this.config.getServerName());
				ListenableFuture<ParamId> retrievalFutureTask = (ListenableFuture<ParamId>) this.comExecutor
						.submit(fetchTask);

				// Create a callback task for the fetch Id task we just created and submit it to
				// the communication executor. The callback is just a wrapper to call back to
				// the methods in this class that are for handling the ParamId having been
				// fetched.
				FutureCallback<ParamId> callback = new ParamIdFetchedCallback(this);
				Futures.addCallback(retrievalFutureTask, callback, this.comExecutor);

				// Finally add the fetch Id task to a list so we can keep track of how many
				// tasks are currently in process
				this.fetchingTaskList.add(retrievalFutureTask);

			} catch (Exception e) {
				logger.error("Exception occurred while executing method addTask", e);
				throw e;
			} finally {
				this.lock.unlock();
			}
		} else {
			try {
				Thread.sleep(30000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			try {
				if (this.noDataLock.tryLock() || this.noDataLock.tryLock(20, TimeUnit.SECONDS)) {
					this.noData = false;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				if (this.noDataLock.isHeldByCurrentThread())
					this.noDataLock.unlock();
			}
		}
	}

	@Override
	public void handleParamIdFetched(ParamId paramId) {
		// This method is for handling the param Id having been fetched from the server.

		this.lock.lock();
		try {
			// Cycle through the list of fetching tasks and remove all that are done.
			// It doesn't matter if it is the same FutureTask that has called this method
			// through its callback. We are only using the lists to count how many are in
			// process in some way.
			Iterator<Future<ParamId>> itr = this.fetchingTaskList.iterator();
			while (itr.hasNext()) {
				Future<ParamId> tsk = itr.next();
				if (tsk.isDone()) {
					itr.remove();
					// Signal that the lists are no longer full.
					this.notFull.signal();
				}
			}

			// If an error didn't somehow occur and we have an Id to process, then let us
			// process it.
			if (paramId != null) {

				// Create a task to calculate and return the param results.
				Callable<ParamResults> calcTask = new CalcParamResultsTask(this.dataSource,
						this.calculators.get(paramId.wavelength), paramId);

				// Submit the calculation task to the calculation executor
				ListenableFuture<ParamResults> calcFutureTask = (ListenableFuture<ParamResults>) this.calcExecutor
						.submit(calcTask);

				// Create a callback to handle the completion of the parameter calculation and
				// submit it to the calculation executor. This is just a wrapper to call the
				// methods in this class that handle the completion of the calculation.
				FutureCallback<ParamResults> calcCallback = new ParamCalculatedCallback(this);
				Futures.addCallback(calcFutureTask, calcCallback, this.calcExecutor);

				// Add the calculation task to the list of calculate tasks
				this.calculatingTaskList.add(calcFutureTask);
			}

		} catch (Exception e) {
			logger.error("Exception occurred while executing method handleParamIdFetched", e);
		} finally {
			this.lock.unlock();
		}

		// If we did get a null, then we should consider this to indicate that we have
		// no data to process.
		if (paramId == null) {
			try {
				if (this.noDataLock.tryLock() || this.noDataLock.tryLock(20, TimeUnit.SECONDS)) {
					this.noData = true;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				if (this.noDataLock.isHeldByCurrentThread())
					this.noDataLock.unlock();
			}
		} else {
			try {
				if (this.noDataLock.tryLock() || this.noDataLock.tryLock(20, TimeUnit.SECONDS)) {
					this.noData = false;
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} finally {
				if (this.noDataLock.isHeldByCurrentThread())
					this.noDataLock.unlock();
			}
		}
	}

	@Override
	public void handleParamsCalculated(ParamResults results) {
		// This method is to handle when parameters have successfully finished
		// calculating.
		this.lock.lock();
		try {

			// Cycle through the list of calculating tasks and remove all that are done.
			// It doesn't matter if it is the same FutureTask that has called this method
			// through its callback. We are only using the lists to count how many are in
			// process in some way.
			Iterator<Future<ParamResults>> itr = this.calculatingTaskList.iterator();
			while (itr.hasNext()) {
				Future<ParamResults> tsk = itr.next();
				if (tsk.isDone()) {
					itr.remove();
					// Signal that the lists are no longer full.
					this.notFull.signal();
				}
			}

			// If an error didn't somehow occur and we have results to process, then let us
			// report them.
			if (results != null) {

				// Create a task to report the param results to the server.
				Callable<Boolean> reportTask = new ReportTask(results, this.config.getPortNum(),
						this.config.getServerName());

				// Submit the report task to the calculation executor
				ListenableFuture<Boolean> reportFutureTask = (ListenableFuture<Boolean>) this.reportComExecutor
						.submit(reportTask);

				// Create a callback to handle the completion of the parameter reporting and
				// submit it to the communication executor. This is just a wrapper to call the
				// methods in this class that handle the completion of reporting.
				FutureCallback<Boolean> reportedCallback = new ParamReportedCallback(this);
				Futures.addCallback(reportFutureTask, reportedCallback, this.reportComExecutor);

				// Add the reporting task to the list of calculate tasks
				this.reportingTaskList.add(reportFutureTask);
			}

		} catch (Exception e) {
			logger.error("Exception occurred while executing method handleParamIdFetched", e);
		} finally {
			this.lock.unlock();
		}

	}

	@Override
	public void handleParamsReported(Boolean done) {
		// This method is to handle when a set of results have successfully been
		// reported to the server.

		this.lock.lock();
		try {
			// Cycle through the list of reporting tasks and remove all that are done.
			// It doesn't matter if it is the same FutureTask that has called this method
			// through its callback. We are only using the lists to count how many are in
			// process in some way.
			Iterator<Future<Boolean>> itr = this.reportingTaskList.iterator();
			while (itr.hasNext()) {
				Future<Boolean> tsk = itr.next();
				if (tsk.isDone()) {
					itr.remove();
					// Signal that the lists are no longer full.
					this.notFull.signal();
				}
			}

		} catch (Exception e) {
			logger.error("Exception occurred while executing method handleParamIdFetched", e);
		} finally {
			this.lock.unlock();
		}

	}

	@Override
	public void handleParamFetchFailed(Throwable arg0) {
		// This method is to handle when fetching a paramId from the server has failed.
		// We will assume that if it failed simply because of network error, it shall be
		// processed again when the server checks to see if everything has been
		// processed. So, we will remove one task from the list and continue on
		// without processing the paramid that should have been returned.

		this.lock.lock();
		try {
			// Cycle through the list of fetching tasks and remove all that are done.
			// It doesn't matter if it is the same FutureTask that has called this method
			// through its callback. We are only using the lists to count how many are in
			// process in some way.
			Iterator<Future<ParamId>> itr = this.fetchingTaskList.iterator();
			while (itr.hasNext()) {
				Future<ParamId> tsk = itr.next();
				if (tsk.isDone()) {
					itr.remove();
					// Signal that the lists are no longer full.
					this.notFull.signal();
				}
			}
		} finally {
			this.lock.unlock();
		}

		if (arg0.getCause() != null && ConnectException.class.isAssignableFrom(arg0.getCause().getClass())) {
			//System.out.println("ConnectException when fetching Id, assuming no data.");
		} else {
			logger.error("Unexpected exception while fetching a new Id", arg0);
		}

		try {
			if (this.noDataLock.tryLock() || this.noDataLock.tryLock(20, TimeUnit.SECONDS)) {
				this.noData = true;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			if (this.noDataLock.isHeldByCurrentThread())
				this.noDataLock.unlock();
		}

	}

	@Override
	public void handleParamsReportedFailed(Throwable arg0) {
		// This method is to handle when reporting results to the server failed.
		// We will assume that if it failed simply because of network error, it shall be
		// processed again when the server checks to see if everything has been
		// processed. So, we will remove one task from the list and continue on
		// as if everything is fine.
		logger.error("Param save Failed,", arg0.getMessage());

		this.lock.lock();
		try {
			// Cycle through the list of reporting tasks and remove all that are done.
			// It doesn't matter if it is the same FutureTask that has called this method
			// through its callback. We are only using the lists to count how many are in
			// process in some way.
			Iterator<Future<Boolean>> itr = this.reportingTaskList.iterator();
			while (itr.hasNext()) {
				Future<Boolean> tsk = itr.next();
				if (tsk.isDone()) {
					itr.remove();
					// Signal that the lists are no longer full.
					this.notFull.signal();
				}
			}

		} finally {
			this.lock.unlock();
		}
	}

	@Override
	public void handleParamsCalculatedFailed(Throwable arg0) {
		// This method is to handle the failure of the calculation task.
		// If it fails somehow, we will just assume it will be processed again at a
		// later date when the server checks to make sure everything has been processed.
		// So we will just drop further processing at this point.
		logger.error("Param calc Failed.", arg0.getMessage());

		this.lock.lock();
		try {
			// Cycle through the list of calculating tasks and remove all that are done.
			// It doesn't matter if it is the same FutureTask that has called this method
			// through its callback. We are only using the lists to count how many are in
			// process in some way.
			Iterator<Future<ParamResults>> itr = this.calculatingTaskList.iterator();
			while (itr.hasNext()) {
				Future<ParamResults> tsk = itr.next();
				if (tsk.isDone()) {
					itr.remove();
					// Signal that the lists are no longer full.
					this.notFull.signal();
				}
			}

		} finally {
			this.lock.unlock();
		}

	}

}
