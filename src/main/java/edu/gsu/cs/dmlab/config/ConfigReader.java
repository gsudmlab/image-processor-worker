/**
 * image-processor-worker, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.config;

import java.io.File;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.logging.log4j.core.LoggerContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.gsu.cs.dmlab.datasources.JP2_FileDataSource;
import edu.gsu.cs.dmlab.datasources.interfaces.IImageDataSource;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.exceptions.InvalidConfigException;
import edu.gsu.cs.dmlab.imageproc.edgedetection.CannyEdgeDetectorOpenImaj;
import edu.gsu.cs.dmlab.imageproc.edgedetection.GradientCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.EntropyParamCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.FractalDimParamCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.KurtosisParamCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.MeanParamCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.RelativeSmoothnessParamCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.SkewnessParamCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.StdDeviationParamCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.TContrastParamCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.TDirectionalityParamCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.UniformityParamCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.interfaces.IMeasures;
import edu.gsu.cs.dmlab.imageproc.imageparam.interfaces.IParamCalculator;
import edu.gsu.cs.dmlab.imageproc.imageparam.util.PeakDetection;
import edu.gsu.cs.dmlab.imageproc.interfaces.IEdgeDetector;
import edu.gsu.cs.dmlab.imageproc.interfaces.IGradientCalculator;
import edu.gsu.cs.dmlab.imageproc.interfaces.IPeakDetector;
import edu.gsu.cs.dmlab.util.Utility;

/**
 * The configuration file reader class. This class reads the configuration file
 * for this project and provides the objects needed for this project that can be
 * configured at runtime through file changes.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ConfigReader {

	private int portnum;
	private int maxThreads;
	private String serverName;
	private String baseDir;
	private int maxCache;
	private int cadenceMin = 6;

	Waveband[] wavelengths = { Waveband.AIA94, Waveband.AIA131, Waveband.AIA171, Waveband.AIA193, Waveband.AIA211,
			Waveband.AIA304, Waveband.AIA335, Waveband.AIA1600, Waveband.AIA1700 };
	Map<Waveband, IParamCalculator[]> calcMap;

	public ConfigReader(String fileLoc, String configName) throws InvalidConfigException {
		this.calcMap = new HashMap<Waveband, IParamCalculator[]>();
		for (Waveband wavelength : wavelengths) {
			this.calcMap.put(wavelength, new IParamCalculator[10]);
		}
		this.config(fileLoc, configName);
	}

	public int getPortNum() {
		return this.portnum;
	}

	public int getMaxThreads() {
		return this.maxThreads;
	}

	public String getServerName() {
		return this.serverName;
	}

	public IParamCalculator[] getCalculators(Waveband wavelength) {
		return this.calcMap.get(wavelength);
	}

	public IImageDataSource getDataSource() {
		IImageDataSource dSource = new JP2_FileDataSource(this.baseDir, this.maxCache, this.cadenceMin, 20);
		return dSource;
	}

	public Waveband[] getWavelengths() {
		return this.wavelengths;
	}

	// /////////////////////////////////////////////////////////////////////////////////
	// Start of private methods
	// ////////////////////////////////////////////////////////////////////////////////
	private void config(String folderLocation, String configName) throws InvalidConfigException {
		try {
			// TODO: still need to implement reading lambda values.
			DocumentBuilderFactory fctry = DocumentBuilderFactory.newInstance();
			Document doc;
			String fileLoc = folderLocation + File.separator + configName;
			DocumentBuilder bldr = fctry.newDocumentBuilder();
			doc = bldr.parse(new File(fileLoc));
			doc.getDocumentElement().normalize();

			Element root = doc.getDocumentElement();
			NodeList ndLst = root.getChildNodes();
			for (int i = 0; i < ndLst.getLength(); i++) {
				Node nde = ndLst.item(i);
				if (nde.getNodeType() == Node.ELEMENT_NODE) {
					String ndName = nde.getNodeName();
					switch (ndName) {
					case "portnum":
						this.portnum = Integer.parseInt(this.getAttrib(nde, "value"));
						break;
					case "threads":
						this.maxThreads = Integer.parseInt(this.getAttrib(nde, "max"));
						break;
					case "servername":
						this.serverName = this.getAttrib(nde, "value");
						break;
					case "paramsettings":
						this.processParams(nde.getChildNodes());
						break;
					case "cachesize":
						this.maxCache = Integer.parseInt(this.getAttrib(nde, "max"));
						break;
					case "basedir":
						this.baseDir = this.getAttrib(nde, "value");
						break;
					case "logger":
						folderLocation = this.getAttrib(nde, "loc");
						String logsfileName = this.getAttrib(nde, "name");
						logsfileName = folderLocation + File.separator + logsfileName;
						this.setupLogging(logsfileName);
						break;
					case "cadence":
						this.cadenceMin = Integer.parseInt(this.getAttrib(nde, "minutes"));
						break;
					}

				}
			}
			System.out.println("Worker: server name: " + this.serverName);
			System.out.println("Worker: port number: " + this.portnum);

		} catch (Exception e) {
			throw new InvalidConfigException("Config failed with: " + e.getMessage());
		}

	}

	private void setupLogging(String logConfFile) {
		Properties props = System.getProperties();
		props.setProperty("log4j.configurationFile", logConfFile);
		File f = new File(logConfFile);
		URI fc = f.toURI();
		LoggerContext.getContext().setConfigLocation(fc);
	}

	private void processParams(NodeList ndList) {

		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndType = nde.getNodeName();
				if (ndType == "param") {
					String ndName = this.getAttrib(nde, "name");
					switch (ndName) {
					case "entropy":
						// Populate position 0 for each wavelength
						this.processEntropy(nde.getChildNodes());
						break;
					case "fdim":
						// Populate position 3 for each wavelength
						this.processFdim(nde.getChildNodes());
						break;
					case "uniformity":
						// Populate position 6 for each wavelength
						this.processUniformity(nde.getChildNodes());
						break;
					case "tdir":
						// Populate position 9 for each wavelength
						this.processTdir(nde.getChildNodes());
						break;
					}
				}
			}
		}

		// At this point, positions 0, 3, 6, 9 should be populated for each wavelength
		// leaving 1, 2, 4, 5, 7, 8.
		this.processRemaining();
	}

	private void processRemaining() {
		IMeasures.PatchSize patchSize = IMeasures.PatchSize._64;
		for (Waveband wavelength : this.wavelengths) {

			// Mean calculator
			IParamCalculator calculator = new MeanParamCalculator(patchSize);
			this.calcMap.get(wavelength)[1] = calculator;

			// Std. Dev. calculator
			calculator = new StdDeviationParamCalculator(patchSize);
			this.calcMap.get(wavelength)[2] = calculator;

			// Skewness calculator
			calculator = new SkewnessParamCalculator(patchSize);
			this.calcMap.get(wavelength)[4] = calculator;

			// Kurtosis calculator
			calculator = new KurtosisParamCalculator(patchSize);
			this.calcMap.get(wavelength)[5] = calculator;

			// Rel. Smoothness
			calculator = new RelativeSmoothnessParamCalculator(patchSize);
			this.calcMap.get(wavelength)[7] = calculator;

			// T. Contrast
			calculator = new TContrastParamCalculator(patchSize);
			this.calcMap.get(wavelength)[8] = calculator;
		}
	}

	private void processTdir(NodeList ndList) {
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "wavelength":
					Waveband wavelength = Utility.getWavebandFromInt(Integer.parseInt(this.getAttrib(nde, "value")));
					IParamCalculator calculator = this.getTDirCalculator(nde.getChildNodes());
					this.calcMap.get(wavelength)[9] = calculator;
					break;
				}
			}
		}
	}

	private IParamCalculator getTDirCalculator(NodeList ndList) {
		IParamCalculator calculator = null;
		int quantizationLevel = 90;
		double threshold = 0;
		int peakwidth = 255;
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "peakwidth":
					peakwidth = Integer.parseInt(this.getAttrib(nde, "value"));
					break;
				case "threshold":
					threshold = Double.parseDouble(this.getAttrib(nde, "value"));
					break;
				}
			}
		}

		IGradientCalculator gradientCalculator = new GradientCalculator("sobel");
		IPeakDetector peakDetector = new PeakDetection(peakwidth, threshold, 0, true);
		calculator = new TDirectionalityParamCalculator(IMeasures.PatchSize._64, gradientCalculator, peakDetector,
				quantizationLevel);
		return calculator;
	}

	private void processUniformity(NodeList ndList) {
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "wavelength":
					Waveband wavelength = Utility.getWavebandFromInt(Integer.parseInt(this.getAttrib(nde, "value")));
					IParamCalculator calculator = this.getUniformityCalculator(nde.getChildNodes());
					this.calcMap.get(wavelength)[6] = calculator;
					break;
				}
			}
		}
	}

	private IParamCalculator getUniformityCalculator(NodeList ndList) {
		IParamCalculator calculator = null;
		int bins = 2;
		double minPixelValue = 0;
		double maxPixelValue = 255;
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "bins":
					bins = Integer.parseInt(this.getAttrib(nde, "value"));
					break;
				}
			}
		}

		calculator = new UniformityParamCalculator(IMeasures.PatchSize._64, bins, minPixelValue, maxPixelValue);
		return calculator;
	}

	private void processFdim(NodeList ndList) {
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "wavelength":
					Waveband wavelength = Utility.getWavebandFromInt(Integer.parseInt(this.getAttrib(nde, "value")));
					IParamCalculator calculator = this.getFdimCalculator(nde.getChildNodes());
					this.calcMap.get(wavelength)[3] = calculator;
					break;
				}
			}
		}
	}

	private IParamCalculator getFdimCalculator(NodeList ndList) {
		IParamCalculator calculator = null;
		float sigma = 2;
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "sigma":
					sigma = Float.parseFloat(this.getAttrib(nde, "value"));
					break;
				}
			}
		}

		IEdgeDetector edgeDetector = new CannyEdgeDetectorOpenImaj(0.02f, 0.08f, sigma);
		calculator = new FractalDimParamCalculator(IMeasures.PatchSize._64, edgeDetector);
		return calculator;
	}

	private void processEntropy(NodeList ndList) {
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "wavelength":
					Waveband wavelength = Utility.getWavebandFromInt(Integer.parseInt(this.getAttrib(nde, "value")));
					IParamCalculator calculator = this.getEntropyCalculator(nde.getChildNodes());
					this.calcMap.get(wavelength)[0] = calculator;
					break;
				}
			}
		}
	}

	private IParamCalculator getEntropyCalculator(NodeList ndList) {
		IParamCalculator calculator = null;
		int bins = 10;
		double minPixelValue = 0;
		double maxPixelValue = 255;
		for (int i = 0; i < ndList.getLength(); i++) {
			Node nde = ndList.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "bins":
					bins = Integer.parseInt(this.getAttrib(nde, "value"));
					break;
				}
			}
		}
		calculator = new EntropyParamCalculator(IMeasures.PatchSize._64, bins, minPixelValue, maxPixelValue);
		return calculator;
	}

	private String getAttrib(Node prntNde, String attName) {
		StringBuffer buf = new StringBuffer("");
		boolean isSet = false;
		if (prntNde.hasAttributes()) {
			NamedNodeMap ndeMp = prntNde.getAttributes();
			for (int i = 0; i < ndeMp.getLength(); i++) {
				Node nde = ndeMp.item(i);
				if (nde.getNodeName().compareTo(attName) == 0) {
					buf.append(nde.getNodeValue());
					isSet = true;
					break;
				}
			}
		}

		if (!isSet) {
			return "";
		} else {
			return buf.toString();
		}
	}

}
