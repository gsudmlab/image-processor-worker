/**
 * image-processor-server, a Library created for use in various projects at the Data Mining Lab 
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).  
 *  
 * Copyright (C) 2019 Georgia State University
 * 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package edu.gsu.cs.dmlab.datatypes;

import java.io.Serializable;

import org.joda.time.DateTime;

/**
 * Class used to send the worker tasks information on what it needs to process
 * and return results for.
 * 
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 *
 */
public class ParamId implements Serializable {

	private static final long serialVersionUID = 3667787085718840126L;

	/**
	 * The id in the database tables used to store the processed results.
	 */
	public int id;

	/**
	 * The waveband of the image to process.
	 */
	public Waveband wavelength;

	/**
	 * The date and time of the image that needs to be processed.
	 */
	public DateTime time;
}
